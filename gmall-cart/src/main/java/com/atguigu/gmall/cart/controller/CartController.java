package com.atguigu.gmall.cart.controller;

import com.atguigu.gmall.cart.interceptor.LoginInterceptor;
import com.atguigu.gmall.cart.pojo.Cart;
import com.atguigu.gmall.cart.service.CartService;
import com.atguigu.gmall.common.bean.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @Date 2021/10/24/0024 20:05
 * @Created by kusi5
 */
@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping
    public String saveCart(Cart cart) {
        this.cartService.saveCart(cart);
        return "redirect:http://cart.gmall.com/addCart.html?skuId=" + cart.getSkuId() + "&count=" + cart.getCount();
    }

    //跳转cart视图
    @GetMapping ("cart.html")
    public String queryCarts(Model model) {
        List<Cart> cartList = this.cartService.queryCarts();
        model.addAttribute("carts", cartList);
        System.out.println("cart.html 进来了");
        return "cart";
    }

    @PostMapping ("updateNum")
    @ResponseBody
    public ResponseVo updateNum(@RequestBody Cart cart) {
        this.cartService.updateNum(cart);
        return ResponseVo.ok();
    }

    @PostMapping ("deleteCart")
    @ResponseBody
    public ResponseVo deleteCart(@RequestParam ("skuId") Long skuId) {
        this.cartService.deleteCart(skuId);
        return ResponseVo.ok();
    }

    @GetMapping("user/{userId}")
    @ResponseBody
    public ResponseVo<List<Cart>> queryCheckedCarts(@PathVariable("userId") Long userId) {
        List<Cart> carts = this.cartService.queryCheckedCarts(userId);
        return ResponseVo.ok(carts);
    }
    /**
     * 新增购物车后的回显方法：skuId count
     *
     * @return
     */
    @GetMapping ("addCart.html")
    public String addCart(Cart cart, Model model) {
        BigDecimal count = cart.getCount();
        cart = this.cartService.queryCartByUserIdAndSkuId(cart.getSkuId());
        cart.setCount(count);
        model.addAttribute("cart", cart);
        return "addCart";
    }

    @GetMapping ("test")
    @ResponseBody
    public String test(HttpServletRequest request) throws ExecutionException, InterruptedException {
        long now = System.currentTimeMillis();
//        System.out.println("controller方法开始执行");
//        this.cartService.execute1();
//        this.cartService.execute2();
//        future1.addCallback(t -> {
//            System.out.println("future1异步任务执行完成：" + t);
//        }, ex -> {
//            System.out.println("future1异步任务出现异常：" + ex.getMessage());
//        });
//        future2.addCallback(t -> {
//            System.out.println("future2异步任务执行完成：" + t);
//        }, ex -> {
//            System.out.println("future2异步任务出现异常：" + ex.getMessage());
//        });
        //System.out.println("子任务的返回结果集：" + future1.get() + "  " + future2.get());
//        System.out.println("controller方法结束执行" + (System.currentTimeMillis() - now));
        System.out.println("获取登录信息：" + LoginInterceptor.getUserInfo());
        return "hello test;";
    }
}
