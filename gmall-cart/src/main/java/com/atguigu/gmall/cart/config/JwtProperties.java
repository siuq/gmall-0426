package com.atguigu.gmall.cart.config;

import com.atguigu.gmall.common.exception.AuthException;
import com.atguigu.gmall.common.utils.RsaUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.security.PublicKey;

@Data
@ConfigurationProperties (prefix = "jwt")
public class JwtProperties {

    private String pubFilePath;
    private String cookieName;
    private String userKey;
    private Integer expire;

    private PublicKey publicKey;

    @PostConstruct
    public void init(){
        try {
            this.publicKey = RsaUtils.getPublicKey(pubFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AuthException("公钥解析失败，请联系运维人员！");
        }
    }
}
