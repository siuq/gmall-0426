package com.atguigu.gmall.index.service;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.index.annotation.GmallCache;
import com.atguigu.gmall.index.feign.GmallPmsClient;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @Date 2021/10/18/0018 19:13
 * @Created by kusi5
 */
@Service
public class IndexService {

    private static final String KEY_PREFIX = "index:cates:";
    private static final String LOCK_PREFIX = "index:cates:lock:";

    @Autowired
    private GmallPmsClient pmsClient;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    private RedissonClient redissonClient;

    public List<CategoryEntity> queryLvl1Categories() {
        ResponseVo<List<CategoryEntity>> listResponseVo = pmsClient.queryCategoriesByPid(0L);
        List<CategoryEntity> responseVoData = listResponseVo.getData();
        return responseVoData;
    }
    @GmallCache (prefix = KEY_PREFIX, timeout = 259200, random = 14400, lock = LOCK_PREFIX)
    public List<CategoryEntity> queryLvl23CategoriesByPid(Long pid) {
        ResponseVo<List<CategoryEntity>> listResponseVo = this.pmsClient.querySubCatesWithSubByPid(pid);
        List<CategoryEntity> categoryEntities = listResponseVo.getData();
        return categoryEntities;
    }
    public List<CategoryEntity> queryLvl23CategoriesByPid2(Long pid) {
        // 1.先查询缓存，如果缓存中命中直接返回
        String json = this.redisTemplate.opsForValue().get(KEY_PREFIX + pid);
        if (StringUtils.isNotBlank(json)) {
            return JSON.parseArray(json, CategoryEntity.class);
        }
        // 为了防止缓存击穿 添加分布式锁
        RLock fairLock = this.redissonClient.getFairLock(LOCK_PREFIX + pid);
        fairLock.lock();

        // 在获取锁的过程中，可能已有请求把数据放入缓存，此时可以再次确认缓存是否存在
        String json2 = this.redisTemplate.opsForValue().get(KEY_PREFIX + pid);
        if (StringUtils.isNotBlank(json2)) {
            return JSON.parseArray(json2, CategoryEntity.class);
        }

        try {
            // 2.如果缓存中没有，则远程调用获取数据，放入缓存
            ResponseVo<List<CategoryEntity>> listResponseVo = this.pmsClient.querySubCatesWithSubByPid(pid);
            List<CategoryEntity> categoryEntities = listResponseVo.getData();

            // 放入缓存
            if (CollectionUtils.isEmpty(categoryEntities)) {
                // 为了防止缓存穿透，缓存为null的数据
                this.redisTemplate.opsForValue().set(KEY_PREFIX + pid, JSON.toJSONString(categoryEntities), 5, TimeUnit.MINUTES);
            } else {
                // 为了防止缓存雪崩，给缓存时间添加随机值
                this.redisTemplate.opsForValue().set(KEY_PREFIX + pid, JSON.toJSONString(categoryEntities), 180 + new Random().nextInt(10), TimeUnit.DAYS);
            }

            return categoryEntities;
        } finally {
            fairLock.unlock();
        }
    }
}
