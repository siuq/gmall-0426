package com.atguigu.gmall.index.controller;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.index.service.IndexService;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Date 2021/10/18/0018 19:10
 * @Created by kusi5
 */
@Controller
public class IndexController {
    @Autowired
    private IndexService indexService;

    @GetMapping(path = {"/", "/**"})
    public String toIndex(Model model){
      List<CategoryEntity> categoryEntities =  indexService.queryLvl1Categories();
      model.addAttribute("categories",categoryEntities);
      return "index";
    }
    @GetMapping("index/cates/{pid}")
    @ResponseBody
    public ResponseVo<List<CategoryEntity>> queryLvl23CategoriesByPid(@PathVariable ("pid")Long pid){
        List<CategoryEntity> categoryEntities = this.indexService.queryLvl23CategoriesByPid(pid);
        return ResponseVo.ok(categoryEntities);
    }
}
