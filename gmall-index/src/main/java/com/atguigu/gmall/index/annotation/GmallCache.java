package com.atguigu.gmall.index.annotation;

import java.lang.annotation.*;

/**
 * @Date 2021/10/20/0020 0:42
 * @Created by kusi5
 */
@Target ({ElementType.METHOD})
@Retention (RetentionPolicy.RUNTIME)
@Documented
public @interface GmallCache {
    /**
     * 缓存key的前缀
     * @return
     */
    String prefix() default "gmall:";

    /**
     * 缓存的过期时间，单位：min
     * 默认5min
     * @return
     */
    int timeout() default 5;

    /**
     * 为了防止缓存雪崩，给过期时间添加随机值
     * 此处可以指定随机值范围，单位：min
     * 默认5min
     * @return
     */
    int random() default 5;

    /**
     * 为了防止缓存击穿，添加分布式锁
     * 此处可以指定分布式锁的前缀
     * @return
     */
    String lock() default "lock:";
}
