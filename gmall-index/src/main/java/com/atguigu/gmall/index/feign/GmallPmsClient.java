package com.atguigu.gmall.index.feign;

import com.atguigu.gmall.pms.api.GmallPmsApi;
import org.springframework.cloud.openfeign.FeignClient;
/**
 * @Date 2021/10/18/0018 19:11
 * @Created by kusi5
 */
@FeignClient ("pms-service")
public interface GmallPmsClient extends GmallPmsApi {
}
