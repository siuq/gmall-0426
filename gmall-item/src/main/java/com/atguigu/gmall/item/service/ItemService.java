package com.atguigu.gmall.item.service;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.common.exception.ItemException;
import com.atguigu.gmall.item.feign.GmallPmsClient;
import com.atguigu.gmall.item.feign.GmallSmsClient;
import com.atguigu.gmall.item.feign.GmallWmsClient;
import com.atguigu.gmall.item.pojo.ItemVo;
import com.atguigu.gmall.pms.entity.*;
import com.atguigu.gmall.pms.entity.Vo.ItemGroupVo;
import com.atguigu.gmall.pms.entity.Vo.SaleAttrValueVo;
import com.atguigu.gmall.sms.vo.ItemSaleVo;
import com.atguigu.gmall.wms.entity.WareSkuEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

/**
 * @Date 2021/10/24/0024 15:27
 * @Created by kusi5
 */
@Service
public class ItemService {

    @Autowired
    private GmallPmsClient pmsClient;

    @Autowired
    private GmallWmsClient wmsClient;

    @Autowired
    private GmallSmsClient smsClient;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private TemplateEngine templateEngine;

//    public ItemVo loadData(Long skuId) {
//        ItemVo itemVo = new ItemVo();
//
//        //1.根据skuId查询sku V
////        CompletableFuture<SkuEntity> skuFuture = CompletableFuture.supplyAsync(() -> {
////            return skuEntity;
////        }, executorService);
//            ResponseVo<SkuEntity> skuEntityResponseVo = this.pmsClient.querySkuById(skuId);
//            SkuEntity skuEntity = skuEntityResponseVo.getData();
//            if (skuEntity == null) {
//                throw new ItemException("你查看的商品不存在");
//            }
//            itemVo.setSkuId(skuEntity.getId());
//            itemVo.setTitle(skuEntity.getTitle());
//            itemVo.setSubTitle(skuEntity.getSubtitle());
//            itemVo.setPrice(skuEntity.getPrice());
//            itemVo.setDefaultImage(skuEntity.getDefaultImage());
//            itemVo.setWeight(skuEntity.getWeight());
//
//        //2.根据三级分类id查询一二三级分类 V
////        CompletableFuture<Void> catesFuture = skuFuture.thenAcceptAsync(skuEntity -> {
////        }, executorService);
//            ResponseVo<List<CategoryEntity>> catesResponseVo = this.pmsClient.queryLvl123CategoriesByCid3(skuEntity.getCategoryId());
//            List<CategoryEntity> categoryEntities = catesResponseVo.getData();
//            itemVo.setCategories(categoryEntities);
//
//        //3.根据品牌id查询品牌 V
////        CompletableFuture<Void> brandFuture = skuFuture.thenAcceptAsync(skuEntity -> {
////        }, executorService);
//            ResponseVo<BrandEntity> brandEntityResponseVo = this.pmsClient.queryBrandById(skuEntity.getBrandId());
//            BrandEntity brandEntity = brandEntityResponseVo.getData();
//            if (brandEntity != null) {
//                itemVo.setBrandId(brandEntity.getId());
//                itemVo.setBrandName(brandEntity.getName());
//            }
//
//        //4.根据spuId查询spu V
////        CompletableFuture<Void> spuFuture = skuFuture.thenAcceptAsync(skuEntity -> {
////        }, executorService);
//            ResponseVo<SpuEntity> spuEntityResponseVo = this.pmsClient.querySpuById(skuEntity.getSpuId());
//            SpuEntity spuEntity = spuEntityResponseVo.getData();
//            if (spuEntity != null) {
//                itemVo.setSpuId(spuEntity.getId());
//                itemVo.setSpuName(spuEntity.getName());
//            }
//
//        //5.根据skuId查询sku的图片列表 V
////        CompletableFuture<Void> imagesFuture = CompletableFuture.runAsync(() -> {
////        }, executorService);
//            ResponseVo<List<SkuImagesEntity>> imagesResponseVo = this.pmsClient.queryImagesBySkuId(skuId);
//            List<SkuImagesEntity> skuImagesEntities = imagesResponseVo.getData();
//            itemVo.setImages(skuImagesEntities);
//
//        //6.根据skuId查询优惠信息 V
////        CompletableFuture<Void> salesFuture = CompletableFuture.runAsync(() -> {
////        }, executorService);
//            ResponseVo<List<ItemSaleVo>> salesResponseVo = this.smsClient.querySalesBySkuId(skuId);
//            List<ItemSaleVo> itemSaleVos = salesResponseVo.getData();
//            itemVo.setSales(itemSaleVos);
//
//        //7.根据skuId查询库存列表 V
////        CompletableFuture<Void> wareFuture = CompletableFuture.runAsync(() -> {
////        }, executorService);
//            ResponseVo<List<WareSkuEntity>> wareResponseVo = this.wmsClient.queryWareSkusBySkuId(skuId);
//            List<WareSkuEntity> wareSkuEntities = wareResponseVo.getData();
//            if (!CollectionUtils.isEmpty(wareSkuEntities)) {
//                itemVo.setStore(wareSkuEntities.stream().anyMatch(wareSkuEntity -> wareSkuEntity.getStock() - wareSkuEntity.getStockLocked() > 0));
//            }
//
//        //8.根据spuId查询spu下所有sku的销售属性： V
////        CompletableFuture<Void> saleAttrsFuture = skuFuture.thenAcceptAsync(skuEntity -> {
////        }, executorService);
//            ResponseVo<List<SaleAttrValueVo>> saleAttrsResponseVo = this.pmsClient.querySaleAttrValuesBySpuId(skuEntity.getSpuId());
//            List<SaleAttrValueVo> saleAttrValueVos = saleAttrsResponseVo.getData();
//            itemVo.setSaleAttrs(saleAttrValueVos);
//
//        //9.根据skuId查询当前sku的销售属性 V {3：'白天白', 4: '12G', 5: '256G'}
////        CompletableFuture<Void> saleAttrFuture = CompletableFuture.runAsync(() -> {
////        }, executorService);
//            ResponseVo<List<SkuAttrValueEntity>> saleAttrResponseVo = this.pmsClient.querySaleAttrValuesBySkuId(skuId);
//            List<SkuAttrValueEntity> skuAttrValueEntities = saleAttrResponseVo.getData();
//            if (!CollectionUtils.isEmpty(skuAttrValueEntities)) {
//                itemVo.setSaleAttr(skuAttrValueEntities.stream().collect(Collectors.toMap(SkuAttrValueEntity::getAttrId, SkuAttrValueEntity::getAttrValue)));
//            }
//
//        //10.根据spuId查询spu下所有销售属性组合与skuId的映射关系  V
////        CompletableFuture<Void> mappingFuture = skuFuture.thenAcceptAsync(skuEntity -> {
////        }, executorService);
//            ResponseVo<String> stringResponseVo = this.pmsClient.queryMappingBySpuId(skuEntity.getSpuId());
//            String json = stringResponseVo.getData();
//            itemVo.setSkuJsons(json);
//
//        //11.根据spuId查询当前商品的描述信息  V
////        CompletableFuture<Void> descFuture = skuFuture.thenAcceptAsync(skuEntity -> {
////        }, executorService);
//            ResponseVo<SpuDescEntity> spuDescEntityResponseVo = this.pmsClient.querySpuDescById(skuEntity.getSpuId());
//            SpuDescEntity descEntity = spuDescEntityResponseVo.getData();
//            if (descEntity != null) {
//                itemVo.setSpuImages(Arrays.asList(StringUtils.split(descEntity.getDecript(), ",")));
//            }
//
//        //12.根据cid结合spuId、skuId查询规格参数分组及组下的规格参数和值
////        CompletableFuture<Void> groupFuture = skuFuture.thenAcceptAsync(skuEntity -> {
////        }, executorService);
//            ResponseVo<List<ItemGroupVo>> groupResponseVo = this.pmsClient.queryGroupsWithAttrValuesByCidAndSpuIdAndSkuId(skuEntity.getCategoryId(), skuEntity.getSpuId(), skuId);
//            List<ItemGroupVo> itemGroupVos = groupResponseVo.getData();
//            itemVo.setGroups(itemGroupVos);
//
////        CompletableFuture.allOf(catesFuture, brandFuture, spuFuture, imagesFuture, salesFuture, wareFuture,
////                saleAttrsFuture, saleAttrFuture, mappingFuture, descFuture, groupFuture).join();
////
////        executorService.execute(() -> {
////            this.generateHtml(itemVo);
////        });
//
//        return itemVo;
//    }
public ItemVo loadData(Long skuId) {
    ItemVo itemVo = new ItemVo();

    //1.根据skuId查询sku V
    CompletableFuture<SkuEntity> skuFuture = CompletableFuture.supplyAsync(() -> {
        ResponseVo<SkuEntity> skuEntityResponseVo = this.pmsClient.querySkuById(skuId);
        SkuEntity skuEntity = skuEntityResponseVo.getData();
        if (skuEntity == null) {
            throw new ItemException("你查看的商品不存在");
        }
        itemVo.setSkuId(skuEntity.getId());
        itemVo.setTitle(skuEntity.getTitle());
        itemVo.setSubTitle(skuEntity.getSubtitle());
        itemVo.setPrice(skuEntity.getPrice());
        itemVo.setDefaultImage(skuEntity.getDefaultImage());
        itemVo.setWeight(skuEntity.getWeight());
        return skuEntity;
    }, executorService);

    //2.根据三级分类id查询一二三级分类 V
    CompletableFuture<Void> catesFuture = skuFuture.thenAcceptAsync(skuEntity -> {
        ResponseVo<List<CategoryEntity>> catesResponseVo = this.pmsClient.queryLvl123CategoriesByCid3(skuEntity.getCategoryId());
        List<CategoryEntity> categoryEntities = catesResponseVo.getData();
        itemVo.setCategories(categoryEntities);
    }, executorService);

    //3.根据品牌id查询品牌 V
    CompletableFuture<Void> brandFuture = skuFuture.thenAcceptAsync(skuEntity -> {
        ResponseVo<BrandEntity> brandEntityResponseVo = this.pmsClient.queryBrandById(skuEntity.getBrandId());
        BrandEntity brandEntity = brandEntityResponseVo.getData();
        if (brandEntity != null) {
            itemVo.setBrandId(brandEntity.getId());
            itemVo.setBrandName(brandEntity.getName());
        }
    }, executorService);

    //4.根据spuId查询spu V
    CompletableFuture<Void> spuFuture = skuFuture.thenAcceptAsync(skuEntity -> {
        ResponseVo<SpuEntity> spuEntityResponseVo = this.pmsClient.querySpuById(skuEntity.getSpuId());
        SpuEntity spuEntity = spuEntityResponseVo.getData();
        if (spuEntity != null) {
            itemVo.setSpuId(spuEntity.getId());
            itemVo.setSpuName(spuEntity.getName());
        }
    }, executorService);

    //5.根据skuId查询sku的图片列表 V
    CompletableFuture<Void> imagesFuture = CompletableFuture.runAsync(() -> {
        ResponseVo<List<SkuImagesEntity>> imagesResponseVo = this.pmsClient.queryImagesBySkuId(skuId);
        List<SkuImagesEntity> skuImagesEntities = imagesResponseVo.getData();
        itemVo.setImages(skuImagesEntities);
    }, executorService);

    //6.根据skuId查询优惠信息 V
    CompletableFuture<Void> salesFuture = CompletableFuture.runAsync(() -> {
        ResponseVo<List<ItemSaleVo>> salesResponseVo = this.smsClient.querySalesBySkuId(skuId);
        List<ItemSaleVo> itemSaleVos = salesResponseVo.getData();
        itemVo.setSales(itemSaleVos);
    }, executorService);

    //7.根据skuId查询库存列表 V
    CompletableFuture<Void> wareFuture = CompletableFuture.runAsync(() -> {
        ResponseVo<List<WareSkuEntity>> wareResponseVo = this.wmsClient.queryWareSkusBySkuId(skuId);
        List<WareSkuEntity> wareSkuEntities = wareResponseVo.getData();
        if (!CollectionUtils.isEmpty(wareSkuEntities)) {
            itemVo.setStore(wareSkuEntities.stream().anyMatch(wareSkuEntity -> wareSkuEntity.getStock() - wareSkuEntity.getStockLocked() > 0));
        }
    }, executorService);

    //8.根据spuId查询spu下所有sku的销售属性： V
    CompletableFuture<Void> saleAttrsFuture = skuFuture.thenAcceptAsync(skuEntity -> {
        ResponseVo<List<SaleAttrValueVo>> saleAttrsResponseVo = this.pmsClient.querySaleAttrValuesBySpuId(skuEntity.getSpuId());
        List<SaleAttrValueVo> saleAttrValueVos = saleAttrsResponseVo.getData();
        itemVo.setSaleAttrs(saleAttrValueVos);
    }, executorService);

    //9.根据skuId查询当前sku的销售属性 V {3：'白天白', 4: '12G', 5: '256G'}
    CompletableFuture<Void> saleAttrFuture = CompletableFuture.runAsync(() -> {
        ResponseVo<List<SkuAttrValueEntity>> saleAttrResponseVo = this.pmsClient.querySaleAttrValuesBySkuId(skuId);
        List<SkuAttrValueEntity> skuAttrValueEntities = saleAttrResponseVo.getData();
        if (!CollectionUtils.isEmpty(skuAttrValueEntities)) {
            itemVo.setSaleAttr(skuAttrValueEntities.stream().collect(Collectors.toMap(SkuAttrValueEntity::getAttrId, SkuAttrValueEntity::getAttrValue)));
        }
    }, executorService);

    //10.根据spuId查询spu下所有销售属性组合与skuId的映射关系  V
    CompletableFuture<Void> mappingFuture = skuFuture.thenAcceptAsync(skuEntity -> {
        ResponseVo<String> stringResponseVo = this.pmsClient.queryMappingBySpuId(skuEntity.getSpuId());
        String json = stringResponseVo.getData();
        itemVo.setSkuJsons(json);
    }, executorService);

    //11.根据spuId查询当前商品的描述信息  V
    CompletableFuture<Void> descFuture = skuFuture.thenAcceptAsync(skuEntity -> {
        ResponseVo<SpuDescEntity> spuDescEntityResponseVo = this.pmsClient.querySpuDescById(skuEntity.getSpuId());
        SpuDescEntity descEntity = spuDescEntityResponseVo.getData();
        if (descEntity != null) {
            itemVo.setSpuImages(Arrays.asList(StringUtils.split(descEntity.getDecript(), ",")));
        }
    }, executorService);

    //12.根据cid结合spuId、skuId查询规格参数分组及组下的规格参数和值
    CompletableFuture<Void> groupFuture = skuFuture.thenAcceptAsync(skuEntity -> {
        ResponseVo<List<ItemGroupVo>> groupResponseVo = this.pmsClient.queryGroupsWithAttrValuesByCidAndSpuIdAndSkuId(skuEntity.getCategoryId(), skuEntity.getSpuId(), skuId);
        List<ItemGroupVo> itemGroupVos = groupResponseVo.getData();
        itemVo.setGroups(itemGroupVos);
    }, executorService);

    CompletableFuture.allOf(catesFuture, brandFuture, spuFuture, imagesFuture, salesFuture, wareFuture,
            saleAttrsFuture, saleAttrFuture, mappingFuture, descFuture, groupFuture).join();

    executorService.execute(() -> {
        this.generateHtml(itemVo);
    });

    return itemVo;
}
    private void generateHtml(ItemVo itemVo){

        // 上下文对象
        Context context = new Context();
        context.setVariable("itemVo", itemVo);

        // IO流 写入文件中
        try (PrintWriter printWriter = new PrintWriter("E:\\workspace\\gmall-0426\\html\\" + itemVo.getSkuId() + ".html");) {
            this.templateEngine.process("item", context, printWriter);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
