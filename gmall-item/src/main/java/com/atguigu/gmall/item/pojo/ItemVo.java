package com.atguigu.gmall.item.pojo;

import com.atguigu.gmall.pms.entity.CategoryEntity;
import com.atguigu.gmall.pms.entity.SkuImagesEntity;
import com.atguigu.gmall.pms.entity.Vo.ItemGroupVo;
import com.atguigu.gmall.pms.entity.Vo.SaleAttrValueVo;
import com.atguigu.gmall.sms.vo.ItemSaleVo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
public class ItemVo {

    // 面包屑
    // 一二三级分类 V
    private List<CategoryEntity> categories;
    // 品牌 V
    private Long brandId;
    private String brandName;
    // spu信息 V
    private Long spuId;
    private String spuName;

    // 主体部分 V
    private Long skuId;
    private String title;
    private String subTitle;
    private BigDecimal price;
    private Integer weight;
    private String defaultImage;

    // 图片列表 V
    private List<SkuImagesEntity> images;

    // 促销信息 V
    private List<ItemSaleVo> sales;

    // 是否有货 V
    private Boolean store = false;

    // 销售属性： V
    // [{attrId: 3, attrName: '机身颜色', attrValues: ['暗夜黑', '白天白']},
    // {attrId: 4, attrName: '机身内存', attrValues: ['8G', '12G']},
    // {attrId: 5, attrName: '机身存储', attrValues: ['128G', '256G']}]
    private List<SaleAttrValueVo> saleAttrs;

    // 当前sku的销售属性：{3：'白天白', 4: '12G', 5: '256G'} V
    private Map<Long, String> saleAttr;

    // 销售属性组合与skuId的映射关系： V
    // {'白天白,8G,128G': 100, '白天白,8G,256G': 200}
    private String skuJsons;

    // 商品描述信息 V
    private List<String> spuImages;

    // 规格参数列表 V
    private List<ItemGroupVo> groups;


}
