package com.atguigu.gmall.item.controller;

import com.atguigu.gmall.item.pojo.ItemVo;
import com.atguigu.gmall.item.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Date 2021/10/24/0024 15:25
 * @Created by kusi5
 */
@Controller
public class ItemController {
    @Autowired
    private ItemService itemService;

    @GetMapping ("{skuId}.html")
    public String loadData(@PathVariable ("skuId")Long skuId, Model model){
        ItemVo itemVo = this.itemService.loadData(skuId);
        model.addAttribute("itemVo", itemVo);
        return "item";
    }
}
