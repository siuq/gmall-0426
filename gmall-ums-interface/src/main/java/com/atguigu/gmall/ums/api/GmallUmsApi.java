package com.atguigu.gmall.ums.api;

import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.ums.entity.UserEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Date 2021/10/20/0020 19:50
 * @Created by kusi5
 */
public interface GmallUmsApi {
    @GetMapping ("ums/user/query")
    public ResponseVo<UserEntity> query(
            @RequestParam ("loginName") String loginName, @RequestParam ("password") String password
    );

    @PostMapping ("ums/user/register")
    public ResponseVo register(UserEntity userEntity, @RequestParam ("code") String code);

    @GetMapping ("ums/user/check/{data}/{type}")
    public ResponseVo<Boolean> checkData(@PathVariable ("data") String data, @PathVariable ("type") Integer type);
}
