package com.atguigu.gmall.scheduled.jobhandler;

import com.atguigu.gmall.scheduled.mapper.CartMapper;
import com.atguigu.gmall.scheduled.pojo.Cart;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @Date 2021/10/27/0027 10:47
 * @Created by kusi5
 */
@Component
public class CartJobHandler {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private CartMapper cartMapper;

    private static final String EXCEPTION_KEY = "cart:exception";

    private static final String KEY_PREFIX = "cart:info:";

    private static final ObjectMapper MAPPER = new ObjectMapper();
    @XxlJob ("cartSyncDataHandler")
    public ReturnT<String> syncData(String param) {
        BoundSetOperations<String, String> setOps = this.redisTemplate.boundSetOps(EXCEPTION_KEY);

        String userId = setOps.pop();

        while (StringUtils.isNotBlank(userId)){
            // 1.删除mysql中的当前用户的购物车
             this.cartMapper.delete(new QueryWrapper<Cart>().eq("user_id", userId));
            // 2.获取redis中的当前用户的所有购物车
            BoundHashOperations<String, Object, Object> hashOps = this.redisTemplate.boundHashOps(KEY_PREFIX + userId);
            List<Object> cartJsons = hashOps.values();
            // 3.判断redis中的购物车是否为空
            if (CollectionUtils.isEmpty(cartJsons)){
                userId= setOps.pop();
                continue;
            }
            cartJsons.forEach(cartJson->{
                try {
                    Cart cart = MAPPER.readValue(cartJson.toString(), Cart.class);
                    this.cartMapper.insert(cart);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });
            userId = setOps.pop();
        }
        return ReturnT.SUCCESS;
    }
}
