package com.atguigu.gmall.pms.entity.Vo;

import lombok.Data;

@Data
public class AttrValueVo {

    private Long attrId;
    private String attrName;
    private String attrValue;
}
