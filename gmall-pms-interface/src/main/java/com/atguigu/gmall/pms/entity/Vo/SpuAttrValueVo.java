package com.atguigu.gmall.pms.entity.Vo;
import com.atguigu.gmall.pms.entity.SpuAttrValueEntity;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import java.util.List;

/**
 * @Date 2021/10/5/0005 16:27
 * @Created by kusi5
 */
public class SpuAttrValueVo extends SpuAttrValueEntity {
    public void setValueSelected(List<String> valueSelected){
        // 如果接受的集合为空，则不设置
        if (CollectionUtils.isEmpty(valueSelected)){
            return;
        }
        this.setAttrValue(StringUtils.join(valueSelected, ","));
    }
}
