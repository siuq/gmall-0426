package com.atguigu.gmall.pms.api;

import com.atguigu.gmall.common.bean.PageParamVo;
import com.atguigu.gmall.common.bean.ResponseVo;
import com.atguigu.gmall.pms.entity.*;
import com.atguigu.gmall.pms.entity.Vo.ItemGroupVo;
import com.atguigu.gmall.pms.entity.Vo.SaleAttrValueVo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Date 2021/10/10/0010 21:17
 * @Created by kusi5
 */

public interface GmallPmsApi {
    @PostMapping ("pms/spu/page")
    ResponseVo<List<SpuEntity>> querySpuByPageJson(@RequestBody PageParamVo pageParamVo);

    @GetMapping ("pms/spu/{id}")
    public ResponseVo<SpuEntity> querySpuById(@PathVariable ("id") Long id);

    @GetMapping ("pms/sku/spu/{spuId}")
    ResponseVo<List<SkuEntity>> querySkusBySpuId(@PathVariable ("spuId") Long spuId);

    @GetMapping ("pms/sku/{id}")
    public ResponseVo<SkuEntity> querySkuById(@PathVariable ("id") Long id);

    @GetMapping ("pms/skuimages/sku/{skuId}")
    public ResponseVo<List<SkuImagesEntity>> queryImagesBySkuId(@PathVariable ("skuId") Long skuId);

    @GetMapping ("pms/category/{id}")
    ResponseVo<CategoryEntity> queryCategoryById(@PathVariable ("id") Long id);

    @GetMapping ("pms/category/parent/{parentId}")
    public ResponseVo<List<CategoryEntity>> queryCategoriesByPid(@PathVariable ("parentId") Long pid);

    @GetMapping ("pms/category/sub/{cid3}")
    public ResponseVo<List<CategoryEntity>> queryLvl123CategoriesByCid3(@PathVariable ("cid3") Long cid3);

    @GetMapping ("pms/category/all/{pid}")
    public ResponseVo<List<CategoryEntity>> querySubCatesWithSubByPid(@PathVariable ("pid") Long pid);

    @GetMapping ("pms/brand/{id}")
    ResponseVo<BrandEntity> queryBrandById(@PathVariable ("id") Long id);

    @GetMapping ("pms/spuattrvalue/category/{cid}")
    ResponseVo<List<SpuAttrValueEntity>> querySearchAttrValuesByCidAndSpuId(
            @PathVariable ("cid") Long cid,
            @RequestParam ("spuId") Long spuId
    );

    @GetMapping ("pms/skuattrvalue/category/{cid}")
    ResponseVo<List<SkuAttrValueEntity>> querySearchAttrValuesByCidAndSkuId(
            @PathVariable ("cid") Long cid,
            @RequestParam ("skuId") Long skuId
    );

    @GetMapping ("pms/skuattrvalue/spu/{spuId}")
    public ResponseVo<List<SaleAttrValueVo>> querySaleAttrValuesBySpuId(@PathVariable ("spuId") Long spuId);

    @GetMapping ("pms/skuattrvalue/sku/{skuId}")
    public ResponseVo<List<SkuAttrValueEntity>> querySaleAttrValuesBySkuId(@PathVariable ("skuId") Long skuId);

    @GetMapping ("pms/skuattrvalue/mapping/{spuId}")
    public ResponseVo<String> queryMappingBySpuId(@PathVariable ("spuId") Long spuId);

    @GetMapping("pms/spudesc/{spuId}")
    public ResponseVo<SpuDescEntity> querySpuDescById(@PathVariable("spuId")Long spuId);

    @GetMapping ("pms/attrgroup/attr/value/{cid}")
    public ResponseVo<List<ItemGroupVo>> queryGroupsWithAttrValuesByCidAndSpuIdAndSkuId(
            @PathVariable ("cid") Long cid,
            @RequestParam ("spuId") Long spuId,
            @RequestParam ("skuId") Long skuId
    );

}
