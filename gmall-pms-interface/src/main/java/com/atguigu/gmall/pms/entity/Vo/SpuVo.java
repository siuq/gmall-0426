package com.atguigu.gmall.pms.entity.Vo;

import com.atguigu.gmall.pms.entity.SpuEntity;
import lombok.Data;

import java.util.List;

/**
 * @Date 2021/10/5/0005 12:35
 * @Created by kusi5
 */
@Data
public class SpuVo extends SpuEntity {
    // 图片信息
    private List<String> spuImages;

    // 基本属性信息
    private List<SpuAttrValueVo> baseAttrs;

    // sku信息
    private List<SkuVo> skus;
}
