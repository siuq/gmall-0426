package com.atguigu.gmall.auth.controller;

import com.atguigu.gmall.auth.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Date 2021/10/20/0020 21:16
 * @Created by kusi5
 */
@Controller
public class AutoController {
    @Autowired
    private AuthService authService;

    @GetMapping("toLogin.html")
    public String toLogin(Model model, @RequestParam(value = "returnUrl", defaultValue = "http://gmall.com")String returnUrl){
        model.addAttribute("returnUrl", returnUrl);
        return "login";
    }
    @PostMapping ("login")
    public String login(
            @RequestParam("returnUrl")String returnUrl,
            @RequestParam("loginName")String loginName,
            @RequestParam("password")String password,
            HttpServletRequest request, HttpServletResponse response
    ){
        this.authService.login(loginName, password, request, response);
        return "redirect:" + returnUrl;
    }
}
