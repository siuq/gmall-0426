package com.atguigu.gmall.auth.feign;

import com.atguigu.gmall.ums.api.GmallUmsApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Date 2021/10/21/0021 14:29
 * @Created by kusi5
 */
@FeignClient ("ums-service")
public interface GmallUmsClient extends GmallUmsApi {
}
