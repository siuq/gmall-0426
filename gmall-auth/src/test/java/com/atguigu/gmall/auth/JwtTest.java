package com.atguigu.gmall.auth;

import com.atguigu.gmall.common.utils.JwtUtils;
import com.atguigu.gmall.common.utils.RsaUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class JwtTest {

    // 别忘了创建D:\\project\rsa目录
	private static final String pubKeyPath = "E:\\workspace\\gmall-0426\\rsa.pub";
    private static final String priKeyPath = "E:\\workspace\\gmall-0426\\rsa.pri";

    private PublicKey publicKey;

    private PrivateKey privateKey;

    @Test
    public void testRsa() throws Exception {
        RsaUtils.generateKey(pubKeyPath, priKeyPath, "234");
    }

    @BeforeEach
    public void testGetRsa() throws Exception {
        this.publicKey = RsaUtils.getPublicKey(pubKeyPath);
        this.privateKey = RsaUtils.getPrivateKey(priKeyPath);
    }

    @Test
    public void testGenerateToken() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("id", "11");
        map.put("username", "liuyan");
        // 生成token
        String token = JwtUtils.generateToken(map, privateKey, 5);
        System.out.println("token = " + token);
    }

    @Test
    public void testParseToken() throws Exception {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpZCI6IjExIiwidXNlcm5hbWUiOiJsaXV5YW4iLCJleHAiOjE2MzQ3MzQyNjN9.WSXfoEAm0dYYHiu_tcxw1vFPU6r6KDXpjieV1V9T7zNNwCn7BecspM1OmdENwAjtB2YcYgZHFb7uXknI4Fe0zFAjxRfhLu5RKQlNJD5hOeA1xI8f0GatPZvYBa4nUT3aCAvzBR_Raza_IcHJvkNYQ21NmqToK4BJpR-UWRv66k7oVVG8jot9-O04UfDJkQa3qhQr7e2Y9RULijOJwuJGTnvmzDOhHYAFSQ6zJ7VzdQm6PcNSieZo8ts7yZ8_EFJi6YDfIOErpVeaEybkSkymmPjKH58YVslyVMDxbFIE2B7yU4_rFMa9xtfxrOr3mwTRquMg3Zm9ADLnqh8EYo5ppQ";
        // 解析token
        Map<String, Object> map = JwtUtils.getInfoFromToken(token, publicKey);
        System.out.println("id: " + map.get("id"));
        System.out.println("userName: " + map.get("username"));
    }
}