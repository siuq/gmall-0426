package com.atguigu.es.demo;

import com.atguigu.es.demo.entity.User;
import com.atguigu.es.demo.services.UserRepository;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class EsDemoApplicationTests {
    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Test
    void contextLoads() {
        elasticsearchRestTemplate.createIndex(User.class);
        elasticsearchRestTemplate.putMapping(User.class);
//        elasticsearchRestTemplate.deleteIndex("user");
    }

    @Autowired
    UserRepository userRepository;

    @Test
    public void testAdd() {
        System.out.println(userRepository.save(new User(1l, "zhang3", 20, "123456")));
    }

    @Test
    public void testDelete() {
        userRepository.deleteById(1l);
    }

    @Test
    public void testQuery() {
        Iterable<User> users = userRepository.findAll();
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    void testAddAll() {
        List<User> users = new ArrayList<>();
        users.add(new User(1l, "柳岩", 18, "123456"));
        users.add(new User(2l, "范冰冰", 19, "123456"));
        users.add(new User(3l, "李冰冰", 20, "123456"));
        users.add(new User(4l, "锋哥", 21, "123456"));
        users.add(new User(5l, "小鹿", 22, "123456"));
        users.add(new User(6l, "韩红", 23, "123456"));
        this.userRepository.saveAll(users);
    }

    @Test
    public void testQueryByAgeRang() {
        System.out.println(userRepository.findByAgeBetween(19, 20));
    }

    @Test
    public void testNative() {
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        queryBuilder.withQuery(QueryBuilders.matchQuery("name", "冰冰"));
        queryBuilder.withSort(SortBuilders.fieldSort("age").order(SortOrder.ASC));
        // 高亮
        queryBuilder.withHighlightBuilder(
                new HighlightBuilder().field("name")
                        .preTags("<em>")
                        .postTags("</em>"));
        // 执行查询，获取分页结果集
        Page<User> userPage = this.userRepository.search(queryBuilder.build());
        // 总页数
        System.out.println(userPage.getTotalPages());
        // 总记录数
        System.out.println(userPage.getTotalElements());
        // 当前页数据
        System.out.println(userPage.getContent());
    }
}
