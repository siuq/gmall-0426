package com.atguigu.es.demo.services;

import com.atguigu.es.demo.entity.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface UserRepository extends
        ElasticsearchRepository<User, Long> {
List<User> findByAgeBetween(Integer age1,Integer age2);
}