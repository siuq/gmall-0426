package com.atguigu.gmall.pms.service;

import com.atguigu.gmall.common.bean.PageParamVo;
import com.atguigu.gmall.common.bean.PageResultVo;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 商品三级分类
 *
 * @author sq
 * @email ${email}
 * @date 2021-09-28 19:50:54
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageResultVo queryPage(PageParamVo paramVo);

    List<CategoryEntity> queryCategoriesByPid(Long parentId);

    List<CategoryEntity> queryCategoriesWithSub(Long pid);

    List<CategoryEntity> queryLvl123CategoriesByCid3(Long cid3);
}

