package com.atguigu.gmall.pms.mapper;

import com.atguigu.gmall.pms.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 19:50:54
 */
@Mapper
public interface BrandMapper extends BaseMapper<BrandEntity> {
	
}
