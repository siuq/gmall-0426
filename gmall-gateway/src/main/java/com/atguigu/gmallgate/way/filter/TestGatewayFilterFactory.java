package com.atguigu.gmallgate.way.filter;

import lombok.Data;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

//@Component
public class TestGatewayFilterFactory extends AbstractGatewayFilterFactory<TestGatewayFilterFactory.KeyValueConfig> {

    public TestGatewayFilterFactory() {
        super(KeyValueConfig.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        //return Arrays.asList("value", "key");
        return Arrays.asList("keys");
    }

    @Override
    public ShortcutType shortcutType() {
        return ShortcutType.GATHER_LIST;
    }

    @Override
    public GatewayFilter apply(KeyValueConfig config) {
        return new GatewayFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                System.out.println("我是一个局部过滤器，拦截特定路由的服务. keys = " + config.keys);
                return chain.filter(exchange);
            }
        };
    }

    @Data
    public static class KeyValueConfig{
        private List<String> keys;
        //private String value;
    }
}
