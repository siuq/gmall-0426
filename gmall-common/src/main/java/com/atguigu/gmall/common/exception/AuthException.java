package com.atguigu.gmall.common.exception;

/**
 * @Date 2021/10/21/0021 14:31
 * @Created by kusi5
 */
public class AuthException extends RuntimeException{
    public AuthException() {
        super();
    }

    public AuthException(String message) {
        super(message);
    }
}
