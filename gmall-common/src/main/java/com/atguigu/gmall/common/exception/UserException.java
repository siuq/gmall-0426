package com.atguigu.gmall.common.exception;

/**
 * @Date 2021/10/20/0020 18:49
 * @Created by kusi5
 */
public class UserException extends RuntimeException{
    public UserException(String msg) {
        super(msg);
    }
}
