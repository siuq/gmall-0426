package com.atguigu.gmall.wms.mapper;

import com.atguigu.gmall.wms.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 20:11:53
 */
@Mapper
public interface WareSkuMapper extends BaseMapper<WareSkuEntity> {
	
}
