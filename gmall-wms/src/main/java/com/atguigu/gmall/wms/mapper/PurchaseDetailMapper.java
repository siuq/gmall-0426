package com.atguigu.gmall.wms.mapper;

import com.atguigu.gmall.wms.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 20:11:53
 */
@Mapper
public interface PurchaseDetailMapper extends BaseMapper<PurchaseDetailEntity> {
	
}
