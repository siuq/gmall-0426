package com.atguigu.gmall.search.feign;

import com.atguigu.gmall.pms.api.GmallPmsApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Date 2021/10/11/0011 0:33
 * @Created by kusi5
 */
@FeignClient("pms-service")
public interface GmallPmsClient extends GmallPmsApi {
    
}
