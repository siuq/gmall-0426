package com.atguigu.gmall.search.entity;

import com.atguigu.gmall.pms.entity.BrandEntity;
import com.atguigu.gmall.pms.entity.CategoryEntity;
import lombok.Data;
import org.elasticsearch.client.xpack.XPackInfoRequest;

import java.util.List;

@Data
public class SearchResponseVo {

    // 品牌过滤列表
    private List<BrandEntity> brands;

    // 分类过滤列表
    private List<CategoryEntity> categories;

    // 规格参数过滤列表
    private List<SearchResponseAttrVo> filters;

    // 总记录数
    private Long total;

    // 当前页码
    private Integer pageNum;
    private Integer pageSize;

    // 当前页的记录
    private List<Goods> goodsList;
}
