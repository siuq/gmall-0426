package com.atguigu.gmall.search.feign;

import com.atguigu.gmall.wms.api.GmallWmsApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Date 2021/10/11/0011 0:34
 * @Created by kusi5
 */
@FeignClient("wms-service")
public interface GmallWmsClient extends GmallWmsApi {
}
