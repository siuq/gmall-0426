package com.atguigu.gmall.search.repository;

import com.atguigu.gmall.search.entity.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @Date 2021/10/11/0011 0:37
 * @Created by kusi5
 */
public interface GoodsRepository extends ElasticsearchRepository<Goods,Long> {
    
}
