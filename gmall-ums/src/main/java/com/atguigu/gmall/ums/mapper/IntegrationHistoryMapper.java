package com.atguigu.gmall.ums.mapper;

import com.atguigu.gmall.ums.entity.IntegrationHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 购物积分记录表
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 20:07:36
 */
@Mapper
public interface IntegrationHistoryMapper extends BaseMapper<IntegrationHistoryEntity> {
	
}
