package com.atguigu.gmall.ums.mapper;

import com.atguigu.gmall.ums.entity.UserCollectShopEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 关注店铺表
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 20:07:36
 */
@Mapper
public interface UserCollectShopMapper extends BaseMapper<UserCollectShopEntity> {
	
}
