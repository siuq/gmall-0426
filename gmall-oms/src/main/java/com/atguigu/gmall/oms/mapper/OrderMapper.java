package com.atguigu.gmall.oms.mapper;

import com.atguigu.gmall.oms.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 19:11:12
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {
	
}
