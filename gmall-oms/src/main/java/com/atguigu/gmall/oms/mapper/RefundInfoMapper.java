package com.atguigu.gmall.oms.mapper;

import com.atguigu.gmall.oms.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 19:11:12
 */
@Mapper
public interface RefundInfoMapper extends BaseMapper<RefundInfoEntity> {
	
}
