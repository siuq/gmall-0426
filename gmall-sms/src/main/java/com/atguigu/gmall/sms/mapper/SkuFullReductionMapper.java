package com.atguigu.gmall.sms.mapper;

import com.atguigu.gmall.sms.entity.SkuFullReductionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品满减信息
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 19:58:48
 */
@Mapper
public interface SkuFullReductionMapper extends BaseMapper<SkuFullReductionEntity> {
	
}
