package com.atguigu.gmall.sms.mapper;

import com.atguigu.gmall.sms.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 19:58:48
 */
@Mapper
public interface HomeAdvMapper extends BaseMapper<HomeAdvEntity> {
	
}
