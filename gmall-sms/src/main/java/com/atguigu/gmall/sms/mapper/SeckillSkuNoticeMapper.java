package com.atguigu.gmall.sms.mapper;

import com.atguigu.gmall.sms.entity.SeckillSkuNoticeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀商品通知订阅
 * 
 * @author sq
 * @email ${email}
 * @date 2021-09-28 19:58:48
 */
@Mapper
public interface SeckillSkuNoticeMapper extends BaseMapper<SeckillSkuNoticeEntity> {
	
}
